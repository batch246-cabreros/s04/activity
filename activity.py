from abc import ABC, abstractmethod
# The import tells the program to get the abc module of python to be used.
# ABC means Abstract Base  Classes - This module provides the infrastructure for defining abstract base classes.

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass
    
    @abstractmethod
    def make_sound(self):
        pass
    
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__() # super() can be used to invoke immediate parent class constructor.
        self._name = name
        self._breed = breed
        self._age = age
    
    # getter
    def get_name(self):
        return self._name
    
    def set_name(self, name):
        self._name = name
        
    def get_breed(self):
        return self._breed
    
    def set_breed(self, breed):
        self._breed = breed
        
    def get_age(self):
        return self._age

    # setter
    def set_age(self, age):
        self._age = age
        
    def eat(self, food):
        print(f"{self._name} is eating {food}.")
        
    def make_sound(self):
        print(f"{self._name} says waaaooww!!.")
        
    def call(self):
        print(f"{self._name} comes to bite you.")

        
class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__() # super() can be used to invoke immediate parent class constructor.
        self._name = name
        self._breed = breed
        self._age = age

    # getter
    def get_name(self):
        return self._name
    
    def set_name(self, name):
        self._name = name
        
    def get_breed(self):
        return self._breed
    
    def set_breed(self, breed):
        self._breed = breed
        
    def get_age(self):
        return self._age

    # setter
    def set_age(self, age):
        self._age = age
        
    def eat(self, food):
        print(f"{self._name} is eating {food}.")
        
    def make_sound(self):
        print(f"{self._name} says roof! roof!!.")
        
    def call(self):
        print(f"{self._name} comes to lick you.")



cat = Cat("Miming", "Puspin", 30)
cat.eat("Bulalo")
cat.make_sound()
cat.call()


dog = Dog("Akyat", "Aspin", 5)
dog.eat("Bibingka")
dog.make_sound()
dog.call()
